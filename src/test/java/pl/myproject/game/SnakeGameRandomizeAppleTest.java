package pl.myproject.game;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SnakeGameRandomizeAppleTest {

    public boolean checkIfAppleOutOfBoundXPoint(Point apple, SnakeGame snakeGame) {

        return apple.getX() > 0 || apple.getX() < snakeGame.getxBound();
    }

    public boolean checkIfAppleOutOfYPointBound(Point apple, SnakeGame snakeGame) {
        return apple.getY() > 0 || apple.getY() < snakeGame.getyBound();
    }

    public boolean checkIfAppleLayOnSnake(Point apple, SnakeGame snakeGame) {
        return !snakeGame.getSnake().contains(apple);
    }

    @Test
    public void randomizeAppleTest() {
        SnakeGame snakeGame = new SnakeGame(10, 10) {
            @Override
            public void onGameEnded() {

            }

            @Override
            public void onNextStep() {

            }
        };
        snakeGame.randomizeApple();
        Point point = snakeGame.getApple();
        assertTrue(checkIfAppleLayOnSnake(point, snakeGame));
        assertTrue(checkIfAppleOutOfBoundXPoint(point, snakeGame));
        assertTrue(checkIfAppleOutOfYPointBound(point, snakeGame));

    }


}

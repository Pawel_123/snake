package pl.myproject.game;

import org.junit.Test;

import static org.junit.Assert.*;

public class PointEqualsTest {

    @Test
    public void samePointTest() {
        Point point1 = new Point(2, 2);
        Point point2 = new Point(2, 2);
        boolean result = point1.equals(point2);
        assertTrue(result);

    }

    @Test
    public void sameObjectTest() {
        Point point1 = new Point(2, 2);
        boolean resullt = point1.equals(point1);
        assertTrue(resullt);
    }

    @Test
    public void whereXPointIsDifferent() {
        Point point1 = new Point(2, 2);
        Point point2 = new Point(3, 2);
        boolean result = point1.equals(point2);
        assertFalse(result);
    }

    @Test
    public void whereYPointIsDifferent() {
        Point point1 = new Point(2, 2);
        Point point2 = new Point(2, 3);
        boolean result = point1.equals(point2);
        assertFalse(result);
    }

    @Test
    public void whereTwoPointAreDifferent(){
        Point point1 = new Point(2,2);
        Point point2 = new Point(3,3);
        boolean result = point1.equals(point2);
        assertFalse(result);
    }

}

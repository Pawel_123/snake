package pl.myproject.game;

import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.*;

public class SnakeContainsTest {
    @Test
    public void wherePointNotContainsOnBodyAndHead() {
        Point point = new Point(2, 2);
        Snake snake = new Snake(new Point(0, 0), Collections.singletonList(new Point(0, 1)));
        boolean result = snake.contains(point);
        assertFalse(result);
    }

    @Test
    public void wherePointLayOnSnakeHead() {
        Point point = new Point(0, 0);
        Snake snake = new Snake(new Point(0, 0), Collections.singletonList(new Point(0, 1)));
        boolean result = snake.contains(point);
        assertTrue(result);
    }

    @Test
    public void wherePointLayOnSnakeBody() {
        Point point = new Point(0, 1);
        Snake snake = new Snake(new Point(0, 0), Collections.singletonList(new Point(0, 1)));
        boolean result = snake.contains(point);
        assertTrue(result);
    }
}

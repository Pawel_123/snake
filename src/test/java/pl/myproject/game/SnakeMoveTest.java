package pl.myproject.game;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class SnakeMoveTest {

    @Test
    public void moveOnLeftTest() {
        Snake snake = new Snake(new Point(2, 2), Arrays.asList(new Point(2, 3), new Point(2, 4)));
        snake.move(Direction.LEFT, true);
        List<Point> bodyresult = snake.getBody();
        Point result = snake.getHead();
        Point expectesPoint = new Point(1, 2);
        List<Point> expectedBody = Arrays.asList(new Point(2, 2), new Point(2, 3));
        assertEquals(expectesPoint, result);
        assertEquals(expectedBody, bodyresult);
    }

    @Test
    public void moveOnRightTest() {
        Snake snake = new Snake(new Point(2, 2), Arrays.asList(new Point(2, 3), new Point(2, 4)));
        snake.move(Direction.RIGHT, true);
        List<Point> bodyresult = snake.getBody();
        Point result = snake.getHead();
        Point expectesPoint = new Point(3, 2);
        List<Point> expectedBody = Arrays.asList(new Point(2, 2), new Point(2, 3));
        assertEquals(expectesPoint, result);
        assertEquals(expectedBody, bodyresult);
    }

    @Test
    public void moveOnUpTest() {
        // given
        Snake snake = new Snake(new Point(2, 2), Arrays.asList(new Point(2, 3), new Point(2, 4)));
        //when
        snake.move(Direction.UP, true);
        //then
        List<Point> bodyresult = snake.getBody();
        Point result = snake.getHead();
        Point expectesPoint = new Point(2, 1);
        List<Point> expectedBody = Arrays.asList(new Point(2, 2), new Point(2, 3));
        assertEquals(expectesPoint, result);
        assertEquals(expectedBody, bodyresult);
    }

    @Test
    public void eatAppleAndMoveOnLeftTest() {
        Snake snake = new Snake(new Point(2, 2), Arrays.asList(new Point(2, 3), new Point(2, 4)));
        snake.move(Direction.LEFT, false);
        List<Point> bodyresult = snake.getBody();
        Point result = snake.getHead();
        Point expectesPoint = new Point(1, 2);
        List<Point> expectedBody = Arrays.asList(new Point(2, 2), new Point(2, 3), new Point(2, 4));
        assertEquals(expectesPoint, result);
        assertEquals(expectedBody, bodyresult);
    }

    @Test
    public void eatAppleAndMoveOnRightTest() {
        Snake snake = new Snake(new Point(2, 2), Arrays.asList(new Point(2, 3), new Point(2, 4)));
        snake.move(Direction.RIGHT, false);
        List<Point> bodyresult = snake.getBody();
        Point result = snake.getHead();
        Point expectesPoint = new Point(3, 2);
        List<Point> expectedBody = Arrays.asList(new Point(2, 2), new Point(2, 3), new Point(2, 4));
        assertEquals(expectesPoint, result);
        assertEquals(expectedBody, bodyresult);
    }

    @Test
    public void eatAppleAndMoveOnUpTest() {
        Snake snake = new Snake(new Point(2, 2), Arrays.asList(new Point(2, 3), new Point(2, 4)));
        snake.move(Direction.UP, false);
        List<Point> bodyresult = snake.getBody();
        Point result = snake.getHead();
        Point expectesPoint = new Point(2, 1);
        List<Point> expectedBody = Arrays.asList(new Point(2, 2), new Point(2, 3), new Point(2, 4));
        assertEquals(expectesPoint, result);
        assertEquals(expectedBody, bodyresult);
    }


}

package pl.myproject.game;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PointMoveTest {

    @Test
    public void moveUpTest(){
        Point point = new Point(2,2);
        point = point.move(Direction.UP);
        Point expectedPoint = new Point(2,1);
        assertEquals(expectedPoint,point);

    }
    @Test
    public void moveDownTest(){
        Point point = new Point(2,2);
        point = point.move(Direction.DOWN);
        Point expectedPoint = new Point(2,3);
        assertEquals(expectedPoint,point);

    }
    @Test
    public void moveLeftTest(){
        Point point = new Point(2,2);
        point = point.move(Direction.LEFT);
        Point expectedPoint = new Point(1,2);
        assertEquals(expectedPoint,point);
    }
    @Test
    public void moveRightTest(){
        Point point = new Point(2,2);
        point = point.move(Direction.RIGHT);
        Point expectedPoint = new Point(3,2);
        assertEquals(expectedPoint,point);

    }
}

package pl.myproject.game;


import java.util.Arrays;
import java.util.List;
import java.util.Random;

public abstract class SnakeGame {
    private Snake snake;
    private Direction direction;
    private int xBound;
    private int yBound;
    private Point apple;

    public SnakeGame(int xBound, int yBound) {
        this.xBound = xBound;
        this.yBound = yBound;
        Point head = new Point(2, 2);
        List<Point> body = Arrays.asList(new Point(3, 2), new Point(3, 3));
        snake = new Snake(head, body);
        direction = Direction.DOWN;
        randomizeApple();

    }

    public Point getApple() {
        return apple;
    }

    public void start() {
        while (!checkIfSnakeCrashed()) {
            onNextStep();
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            boolean appleEaten = snake.getHead().move(direction).equals(apple);
            snake.move(direction, !appleEaten);
            if (appleEaten)
                randomizeApple();
        }
        onGameEnded();
    }

    public abstract void onGameEnded();

    public abstract void onNextStep();

    public void randomizeApple() {
        Random random = new Random();
        do {
            apple = new Point(random.nextInt(xBound), random.nextInt(yBound));

        }

        while (snake.contains(apple));
    }

    private boolean checkIfSnakeCrashed() {
        Point head = snake.getHead();
        return checkIfPointIsOutOfBounds(head) || snake.getBody().contains(head);

    }

    private boolean checkIfPointIsOutOfBounds(Point head) {
        return head.getX() < 0 || head.getY() < 0 || head.getY() >= yBound || head.getX() >= xBound;
    }

    public Snake getSnake() {
        return snake;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public int getxBound() {
        return xBound;
    }

    public int getyBound() {
        return yBound;
    }
}

package pl.myproject.game;

public enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
}

package pl.myproject.game;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        SnakeGame snakeGame = new SnakeGame(10, 10) {
            @Override
            public void onGameEnded() {
                System.out.println("Gra się skończyła");
            }

            @Override
            public void onNextStep() {
                System.out.println(getSnake());
            }
        };
        snakeGame.start();
    }
}

package pl.myproject.ui;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import pl.myproject.game.Direction;
import pl.myproject.game.Point;
import pl.myproject.game.Snake;
import pl.myproject.game.SnakeGame;

public class Controller {
    private final static int POINT_SIZE = 20;
    private SnakeGame snakeGame;
    private GraphicsContext graphicsContext;

    @FXML
    private Canvas canvas;

    @FXML
    private Label gameEndedLabel;

    @FXML
    private GridPane buttonsGridPane;

    public void initialize() {
        gameEndedLabel.setManaged(false);
        graphicsContext = canvas.getGraphicsContext2D();
        snakeGame = new SnakeGame((int) canvas.getWidth() / POINT_SIZE, (int) canvas.getHeight() / POINT_SIZE) {
            @Override
            public void onGameEnded() {
                gameEndedLabel.setManaged(true);
                buttonsGridPane.setManaged(false);
                buttonsGridPane.setVisible(false);


                Platform.runLater(() -> {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Gra zakończona");
                    alert.setHeaderText(null);
                    alert.setContentText("Gra się zakończyła");

                    alert.showAndWait();
                });

            }

            @Override
            public void onNextStep() {
                drawGame();
            }

        };
        Thread thread = new Thread(() -> snakeGame.start());
        thread.setDaemon(true);
        thread.start();
    }

    private void drawGame() {
        graphicsContext.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        graphicsContext.setFill(Color.RED);
        Snake snake = snakeGame.getSnake();
        drawPoint(snake.getHead());
        graphicsContext.setFill(Color.GREEN);
        snake.getBody().forEach(this::drawPoint);
        graphicsContext.setFill(Color.YELLOW);
        drawPoint(snakeGame.getApple());
    }

    private void drawPoint(Point point) {
        graphicsContext.fillRect(point.getX() * POINT_SIZE, point.getY() * POINT_SIZE, POINT_SIZE, POINT_SIZE);
    }

    public void onLeftButtonClick() {
        snakeGame.setDirection(Direction.LEFT);
    }

    public void onRightButtonClick() {
        snakeGame.setDirection(Direction.RIGHT);
    }

    public void onUpButtonClick() {
        snakeGame.setDirection(Direction.UP);
    }

    public void onDownButtonClick() {
        snakeGame.setDirection(Direction.DOWN);
    }

}
